#include<iostream>
#include"IStructure.h"

#ifndef HeapMax_h
#define HeapMax_h

class HeapMax : public IStructure {

    private:
        int* _heapArray;
        int _size;
        std::string _cr, _cp, _cl;

        bool _ContainsRecursively(int value, int index);
        void _HeapMaxFixDown(int index);
        void _HeapMaxFixUp(int index);
        void _PrintGraph(std::string sp, std::string sn, int index);

    public:
        HeapMax();
        ~HeapMax();

        int AddFromFile(int value);
        bool ContainsLinear(int value);
        bool ContainsRecursively(int value);
        void DeleteStructure();
        int Pop();
        void PrintArray();
        void PrintGraph();
        int Push(int value);
};


#endif
