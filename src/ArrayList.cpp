#include"ArrayList.h"

ArrayList::ArrayList() {

    _array = NULL;
    _size = 0;

}

ArrayList::~ArrayList() {

    DeleteStructure();

}


/* Metoda dodajaca element na koniec listy - za kazdym razem tworzy nowa tablice
   o rozmiarze o jeden wiekszym i przenosi do niej reszte elementow

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowa tablice
*/
int ArrayList::AddBack(int value) {

    if(_array == NULL) {

        _array = new (std::nothrow) int[1];
        if(_array == NULL) return 0;
        else {
            _size = 1;
            _array[0] = value;
        }
    }
    else {

        int newSize = _size + 1;
        int* temp = new (std::nothrow) int[newSize];
        if(temp == NULL) return 0;
        else {
            temp[_size] = value;
            for(int i = 0; i < _size; i++) {

                temp[i] = _array[i];
            }

            delete[] _array;
            _array = temp;
            _size = newSize; 
        }

    }

    return 1;
}


/* Metoda uzywana podczas tworzenia struktury z pliku
   implementuje metode z interfejsu IStructure

   zwraca to samo co metoda wywolywana przez nia - AddBack
*/
int ArrayList::AddFromFile(int value) {

    return AddBack(value);
}


/* Metoda dodajaca element na poczatek listy - za kazdym razem tworzy nowa tablice
   o rozmiarze o jeden wiekszym i przenosi do niej reszte elementow

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowa tablice
*/
int ArrayList::AddFront(int value) {

    if(_array == NULL) {

        _array = new (std::nothrow) int[1];
        if(_array == NULL) return 0;
        else {
            _size = 1;
            _array[0] = value;
        }
    }
    else {

        int newSize = _size + 1;
        int* temp = new (std::nothrow) int[newSize];
        if(temp == NULL) return 0;
        else {
            temp[0] = value;
            for(int i = 1; i < newSize; i++) {

                temp[i] = _array[i - 1];
            }

            delete[] _array;
            _array = temp;
            _size = newSize; 
        }

    }

    return 1;

}


/* Metoda dodajaca element na wybrana pozycje - za kazdym razem tworzy nowa tablice
   o rozmiarze o jeden wiekszym i przenosi do niej reszte elementow

   Pozycje numerujemy od ZERA tak jak w tablicach

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowa tablice
   zwraca -1 jezelie probujesz dodac element na bledna pozycje - < 0 lub > _size
*/
int ArrayList::AddOnPosition(int value, int position) {

    if(position >= 0 && position <= _size) {

        if(position == 0) {
            if(AddFront(value) == 0) return 0;
        }
        else if(position == _size) {
            if(AddBack(value) == 0) return 0;
        }
        else {

            int newSize = _size + 1;
            int* temp = new (std::nothrow) int[newSize];
            if(temp == NULL) return 0;
            else {
                for(int i = 0; i < position; i++) {

                    temp[i] = _array[i];
                }


                temp[position] = value;

                for(int i = position; i < _size; i++) {

                    temp[i + 1] = _array[i];
                }

                delete[] _array;
                _array = temp;
                _size = newSize;
            }
        }

    }
    else {

        //chcemy dodac element na bledna pozycje
        return -1;

    }

    return 1;
    
}


/* Metoda sprawdzajaca czy w lisicie znajduje sie element o wskazanej wartosci,
   jezeli tak zwraca true, w przeciwnym razie false
*/
bool ArrayList::Contains(int value) {

    if(GetItemPosition(value) != -1)
        return true;
    else
        return false;

}


/* Usuwa cala liste */
void ArrayList::DeleteStructure() {

    delete[] _array;
    _array = NULL;  
    _size = 0;  
}

/* Metoda znajdujaca pierwszy element w tablicy o zadanej wartosci i zwracajaca jego numer
   w tablicy liczonej od zera, jesli nie ma takiego elementu zwraca -1
*/
int ArrayList::GetItemPosition(int value) {
    
    for(int i = 0; i < _size; i++)
        if(_array[i] == value)
            return i;

    return -1;

}


/* Metoda wypisujaca zawartosc listy na stdout */
void ArrayList::Print() {

    for(int i = 0; i < _size; i++)
        std::cout << _array[i] << " ";

    std::cout << std::endl;

}

/* Metoda usuwająca ostatni element z listy - za kazdym razem tworzy nowa tablice
   o rozmiarze o jeden mniejsza i przenosi do niej reszte elementow

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie usuwania, nie uda jej sie zaalokowac miejsca na nowa tablice
   zwraca -1 jezeli probujesz usunac ostatni element z pustej listy
*/
int ArrayList::RemoveBack() {

    if(_size == 1) {

        delete[] _array;
        _array = NULL;
        _size = 0;
    }
    else if(_size > 1) {

        int newSize = _size - 1;
        int* temp = new (std::nothrow) int [newSize];
        
        if(temp == NULL) return 0;
        else {

            for(int i = 0; i < newSize; i++) {

                temp[i] = _array[i];
            }

            delete[] _array;
            _array = temp;
            _size = newSize;

        }

    }
    else {

        //usuwanie z pustej listy
        return -1;
       
    }

    return 1;

}


/* Metoda usuwająca pierwszy element z listy - za kazdym razem tworzy nowa tablice
   o rozmiarze o jeden mniejsza i przenosi do niej reszte elementow

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie usuwania, nie uda jej sie zaalokowac miejsca na nowa tablice
   zwraca -1 jezeli probujesz usunac ostatni element z pustej listy
*/
int ArrayList::RemoveFront() {

    if(_size == 1) {

        delete[] _array;
        _array = NULL;
        _size = 0;
    }
    else if(_size > 1) {

        int newSize = _size - 1;
        int* temp = new (std::nothrow) int[newSize];

        if(temp == NULL) return 0;
        else {

            for(int i = 1; i < _size; i++) {

                temp[i - 1] = _array[i];
            }

            delete[] _array;
            _array = temp;
            _size = newSize; 

        }

    }
    else {

        //usuwanie z pustej listy
        return -1;
    
    }

    return 1;

}


/* Metoda usuwajaca element z wybranej pozycji - za kazdym razem tworzy nowa tablice
   o rozmiarze o jeden mniejszym i przenosi do niej reszte elementow

   Pozycje numerujemy od ZERA tak jak w tablicach

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie usuwania, nie uda jej sie zaalokowac miejsca na nowa tablice
   zwraca -1 jezelie probujesz usunac element z blednej pozycji - < 0 lub >= _size
*/
int ArrayList::RemoveFromPosition(int position) {

    if(position >= 0 && position < _size) {

        if(position == 0) {

            //sprawdzanie -1 dla RemoveFront nie jest konieczne 
            //bo z tego miejsca nie da sie doprowadzic do takiej sytuacji
            if(RemoveFront() == 0) return 0;
        }
        else if(position == _size - 1) {

            //sprawdzanie -1 dla RemoveFront nie jest konieczne 
            //bo z tego miejsca nie da sie doprowadzic do takiej sytuacji
            if(RemoveBack() == 0) return 0;
        }
        else {

            int newSize = _size - 1;
            int* temp = new (std::nothrow) int[newSize];
            if(temp == NULL) return 0;
            else {

                for(int i = 0; i < position; i++) {

                    temp[i] = _array[i];
                }
                        
                for(int i = position + 1; i < _size; i++) {

                    temp[i - 1] = _array[i];
                }

                delete[] _array;
                _array = temp;
                _size = newSize;
            }

        }
    }
    else {

        //usuwanie z niedozwolonej pozycji
        return -1;

    }

    return 1;    
}


/* Metoda usuwająca pierwszy napotkany element z listy o zadanej wartosci - za kazdym razem tworzy nowa tablice
   o rozmiarze o jeden mniejsza i przenosi do niej reszte elementow

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie usuwania, nie uda jej sie zaalokowac miejsca na nowa tablice
   zwraca -1 jezeli probujesz usunac element ktorego nie ma w liscie
*/
int ArrayList::RemoveValue(int value) {

    int position = GetItemPosition(value);
    if(position == -1) return -1;
    if(RemoveFromPosition(position) == 0) return 0;
    //sprawdzanie dla RemoveFromPosition dla -1 nie jest konieczne
    //bo taka sytuacja z tego miejsca nigdy nie nastapi

    return 1;
}
