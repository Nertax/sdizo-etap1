#include<iostream>
#include"IList.h"
#include"IStructure.h"

#ifndef ArrayList_h
#define ArrayList_h

class ArrayList : public IList, public IStructure {

    private:
        int* _array;
        int _size;
        

    public:
        ArrayList();
        ~ArrayList();

        int AddBack(int value);
        int AddFromFile(int value);
        int AddFront(int value);
        int AddOnPosition(int value, int position);
        bool Contains(int value);
        void DeleteStructure();
        int GetItem(int position) { return _array[position]; }
        int GetItemPosition(int value);
        int GetSize() { return _size; }
        void Print();
        int RemoveBack();
        int RemoveFront();
        int RemoveFromPosition(int position);
        int RemoveValue(int value);
};

#endif