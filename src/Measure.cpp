#include"Measure.h"

void Measure::Start() {

    _PrintStart();
    _MeasureMenuLoop();
}


//glowna petla measure menu
void Measure::_MeasureMenuLoop() {

    while(1) {

        std::string action;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);


        if(action == "h" || action == "help")
            _PrintHelp();

        else if(action == "a" || action == "arraylist")
            _MeasureArrayListMenu();

        else if(action == "l" || action == "linkedlist")
            _MeasureLinkedListMenu();

        else if(action == "b" || action == "bstree")
            _MeasureBSTreeMenu();

        else if(action == "heap")
            _MeasureHeapMenu();

        else if(action == "auto")
            _AutoMeasurements();

        else if(action == "q" || action == "quit")
            return;

        else
            _PrintNoSuchAction();

    }


}

void Measure::_MeasureArrayListMenu() {


    while(1) {

        std::string action, fileName;
        int numberOfMeasurements, structureSize;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintArrayListHelp(); 

        else if(action == "ab" || action == "addback") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(AddBack));
            std::cout << "Pomiar zakonczony." << std::endl;

        }

        else if(action == "af" || action == "addfront") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(AddFront));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

        else if(action == "ap" || action == "addonposition") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(AddOnPosition));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

        else if(action == "c" || action == "contains") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(Contains));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }

        else if(action == "rf" || action == "removefront") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveFront));
            std::cout << "Pomiar zakonczony." << std::endl;
            
        }

        else if(action == "rb" || action == "removeback") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveBack));
            std::cout << "Pomiar zakonczony." << std::endl;
            
        }

        else if(action == "rp" || action == "removefromposition") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveFromPosition));
            std::cout << "Pomiar zakonczony." << std::endl;
            
        }

       else if(action == "rv" || action == "removevalue") {

            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureArrayList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveValue));
            std::cout << "Pomiar zakonczony." << std::endl;
        }        
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }
}

void Measure::_MeasureLinkedListMenu() {


    while(1) {

        std::string action, fileName;
        int numberOfMeasurements, structureSize;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintLinkedListHelp();

        else if(action == "ab" || action == "addback") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(AddBack));
            std::cout << "Pomiar zakonczony." << std::endl;

        }

        else if(action == "af" || action == "addfront") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(AddFront));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

        else if(action == "ap" || action == "addonposition") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(AddOnPosition));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

        else if(action == "c" || action == "contains") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(Contains));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }

        else if(action == "rf" || action == "removefront") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveFront));
            std::cout << "Pomiar zakonczony." << std::endl;
            
        }

        else if(action == "rb" || action == "removeback") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveBack));
            std::cout << "Pomiar zakonczony." << std::endl;
            
        }

        else if(action == "rp" || action == "removefromposition") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveFromPosition));
            std::cout << "Pomiar zakonczony." << std::endl;
            
        }

       else if(action == "rv" || action == "removevalue") {

            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureLinkedList(numberOfMeasurements, structureSize, fileName, MeasuerIList(RemoveValue));
            std::cout << "Pomiar zakonczony." << std::endl;
        }        
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }
}

void Measure::_MeasureHeapMenu() {


    while(1) {

        std::string action, fileName;
        int numberOfMeasurements, structureSize;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintHeapHelp();

        else if(action == "pu" || action == "push")  {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureHeap(numberOfMeasurements, structureSize, fileName, MeasuerHeap(Push));
            std::cout << "Pomiar zakonczony." << std::endl;

        }

        else if(action == "po" || action == "pop") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureHeap(numberOfMeasurements, structureSize, fileName, MeasuerHeap(Pop));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

       else if(action == "cr" || action == "containsrecursively") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureHeap(numberOfMeasurements, structureSize, fileName, MeasuerHeap(ContainsRecursively));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

        else if(action == "cl" || action == "containslinear") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureHeap(numberOfMeasurements, structureSize, fileName, MeasuerHeap(ContainsLinear));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }      
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }
}


void Measure::_MeasureBSTreeMenu() {


    while(1) {

        std::string action, fileName;
        int numberOfMeasurements, structureSize;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintBSTreeHelp();

        else if(action == "ar" || action == "addrecursively")  {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureBSTree(numberOfMeasurements, structureSize, fileName, MeasuerBSTree(AddRecursively));
            std::cout << "Pomiar zakonczony." << std::endl;

        }

        else if(action == "al" || action == "addloop") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureBSTree(numberOfMeasurements, structureSize, fileName, MeasuerBSTree(AddLoop));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

       else if(action == "c" || action == "contains") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureBSTree(numberOfMeasurements, structureSize, fileName, MeasuerBSTree(ContainsBSTree));
            std::cout << "Pomiar zakonczony." << std::endl;
         
        }

        else if(action == "r" || action == "remove") {
            std::cin >> numberOfMeasurements >> structureSize >> fileName;
            _ExecuteMeasureBSTree(numberOfMeasurements, structureSize, fileName, MeasuerBSTree(Remove));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }      
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }
}

//napis helpa z menu array listy
void Measure::_PrintArrayListHelp() {


    std::cout << "Aktualnie wykonujesz pomiary na ArrayList (tablica), aby wykonac pomiar musisz podac cztery argumenty oddzielone bialym znakiem: " << std::endl
              << "pierwszy to rodzaj operacji, dostepne operacje to:" << std::endl
              << "  AddFront - wpisz af/addfront" << std::endl
              << "  AddBack - wpisz ab/addback" << std::endl
              << "  AddOnPosition - wpisz ap/addonposition" << std::endl
              << "  Contains - wpisz c/contains i podaj liczbe" << std::endl
              << "  RemoveFront - wpisz rf/removefront" << std::endl
              << "  RemoveBack - wpisz rb/removeback" << std::endl
              << "  RemoveFromPosition - wpisz rp/removefromposition" << std::endl
              << "  RemoveValue - wpisz rv/removevalue i podaj liczbe" << std::endl
              << "drugi to ilosc powtorzen pomiaru" << std::endl
              << "trzeci to wielkosc struktury na ktorej chcemy wykonac operacje" << std::endl
              << "czwarty to nazwa pliku do ktorego zapisane zostana wyniki pomiarow" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;


}

//napis helpa z menu linked listy
void Measure::_PrintLinkedListHelp() {


    std::cout << "Aktualnie wykonujesz pomiary na LinkedList (lista dwukierunkowa), aby wykonac pomiar musisz podac cztery argumenty oddzielone bialym znakiem: " << std::endl
              << "pierwszy to rodzaj operacji, dostepne operacje to:" << std::endl
              << "  AddFront - wpisz af/addfront" << std::endl
              << "  AddBack - wpisz ab/addback" << std::endl
              << "  AddOnPosition - wpisz ap/addonposition" << std::endl
              << "  Contains - wpisz c/contains i podaj liczbe" << std::endl
              << "  RemoveFront - wpisz rf/removefront" << std::endl
              << "  RemoveBack - wpisz rb/removeback" << std::endl
              << "  RemoveFromPosition - wpisz rp/removefromposition" << std::endl
              << "  RemoveValue - wpisz rv/removevalue i podaj liczbe" << std::endl
              << "drugi to ilosc powtorzen pomiaru" << std::endl
              << "trzeci to wielkosc struktury na ktorej chcemy wykonac operacje" << std::endl
              << "czwarty to nazwa pliku do ktorego zapisane zostana wyniki pomiarow" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;


}


//napis helpa z menu heap
void Measure::_PrintHeapHelp() {


    std::cout << "Aktualnie wykonujesz pomiary na Heap (kopiec), aby wykonac pomiar musisz podac cztery argumenty oddzielone bialym znakiem: " << std::endl
              << "pierwszy to rodzaj operacji, dostepne operacje to:" << std::endl
              << "  Push - wpisz pu/push" << std::endl
              << "  Pop - wpisz po/pop" << std::endl
              << "  ContainsRecursively - wpisz cr/containsrecursively" << std::endl
              << "  ContainsLinear - wpisz cl/containslinear" << std::endl
              << "drugi to ilosc powtorzen pomiaru" << std::endl
              << "trzeci to wielkosc struktury na ktorej chcemy wykonac operacje" << std::endl
              << "czwarty to nazwa pliku do ktorego zapisane zostana wyniki pomiarow" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;

}

//napis helpa z menu BSTree
void Measure::_PrintBSTreeHelp() {


    std::cout << "Aktualnie wykonujesz pomiary na BSTree (binarne drzewo poszukiwan), aby wykonac pomiar musisz podac cztery argumenty oddzielone bialym znakiem: " << std::endl
              << "pierwszy to rodzaj operacji, dostepne operacje to:" << std::endl
              << "  AddRecursively - wpisz ar/addrecursively" << std::endl
              << "  AddLoop - wpisz al/addloop" << std::endl
              << "  Contains - wpisz c/contains" << std::endl
              << "  Remove - wpisz r/remove" << std::endl
              << "drugi to ilosc powtorzen pomiaru" << std::endl
              << "trzeci to wielkosc struktury na ktorej chcemy wykonac operacje" << std::endl
              << "czwarty to nazwa pliku do ktorego zapisane zostana wyniki pomiarow" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;

}


//napis startowy w glownym menu
void Measure::_PrintStart() {

    std::cout << "Program zostal uruchomiony w trybie pomiarow. " << std::endl
              << "Mozna w nim mierzyc czas wykonywania pooszczegolnych operacji, dla wybranej wielkosci problemu." << std::endl
              << "Aby wyswietlic pomoc i dowiedziec sie wiecej wpisz h/help" << std::endl << std::endl;
    
}

//napis ze nie ma takiej funkcji
void Measure::_PrintNoSuchAction() {

    std::cout << "Nie znaleziono takiej akcji. Prosze wybrac ponownie." << std::endl
              << "Polecenie h/help wyswietla pomoc." << std::endl;

}

//napis helpa z glownego menu
void Measure::_PrintHelp() {

    std::cout << "Nalezy wybrac strukture danych na ktorej bedziemy wykonywac pomiary: " << std::endl
              << "ArrayList (tablica) - wpisz a/arraylist" << std::endl
              << "LinkedList (list dwukierunkowa) - wpisz l/linkedlist" << std::endl
              << "BSTree (binarne drzewo poszukiwan) - wpisz b/bstree" << std::endl
              << "Heap (kopiec) - wpisz heap" << std::endl
              << "Aby wykonac automatyczne pomiary - wpisz auto" << std::endl
              << "Aby wyjsc z programu wpisz q/quit" << std::endl << std::endl;
}

//napis przed automatycznymi pomiarami
void Measure::_PrintAutoMeasurements() {

    std::cout << "Zostaly uruchomione automatyczne pomiary. Czas zbierania danch pomiarowych moze wyniesc kilka minut. Dane zostana wygenerowane do plikow nazywanych w nastepujacy sposob - x_y_z, gdzie:" << std::endl
              << "x oznacza rodzaj struktury, odpowiednio a - arraylist, l - linkedlist, h - heap, b - bst" << std::endl
              << "y - oznacza operacje na danej strukturze, nazwy zgodne ze skrotami, przy recznych testach" << std::endl
              << "z - oznacza ilosc powtorzen dla kazdego rozmiaru struktury - usrednianie wyniku" << std::endl
              << "kazdy pomiar zostanie wykonany dla nastepujacych wielkosci struktur: 100, 500, 1000, 2000, 5000, 7500, 10000, 12500, 15000" << std::endl
              << "Czekaj na komunikat o zakonczeniu pomiarow" << std::endl;
}
  

void Measure::_AutoMeasurements() {

    _PrintAutoMeasurements();

    int numberOfMeasurements = 100;

    ArrayList structureSizeList;
    structureSizeList.AddBack(100);
    structureSizeList.AddBack(500);
    structureSizeList.AddBack(1000);
    structureSizeList.AddBack(2000);
    structureSizeList.AddBack(5000);
    structureSizeList.AddBack(7500);
    structureSizeList.AddBack(10000);
    structureSizeList.AddBack(12500);
    structureSizeList.AddBack(15000);


    for(int i = 0; i < structureSizeList.GetSize(); i++) {
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_ab_100", MeasuerIList(AddBack));
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_af_100", MeasuerIList(AddFront));
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_ap_100", MeasuerIList(AddOnPosition));
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_c_100", MeasuerIList(Contains));
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_rb_100", MeasuerIList(RemoveBack));
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_rf_100", MeasuerIList(RemoveFront));
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_rp_100", MeasuerIList(RemoveFromPosition));
        _ExecuteMeasureArrayList(numberOfMeasurements, structureSizeList.GetItem(i), "a_rv_100", MeasuerIList(RemoveValue));

        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_ab_100", MeasuerIList(AddBack));
        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_af_100", MeasuerIList(AddFront));
        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_ap_100", MeasuerIList(AddOnPosition));
        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_c_100", MeasuerIList(Contains));
        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_rb_100", MeasuerIList(RemoveBack));
        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_rf_100", MeasuerIList(RemoveFront));
        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_rp_100", MeasuerIList(RemoveFromPosition));
        _ExecuteMeasureLinkedList(numberOfMeasurements, structureSizeList.GetItem(i), "l_rv_100", MeasuerIList(RemoveValue)); 

        _ExecuteMeasureHeap(numberOfMeasurements, structureSizeList.GetItem(i), "h_pu_100", MeasuerHeap(Push)); 
        _ExecuteMeasureHeap(numberOfMeasurements, structureSizeList.GetItem(i), "h_po_100", MeasuerHeap(Pop));
        _ExecuteMeasureHeap(numberOfMeasurements, structureSizeList.GetItem(i), "h_cr_100", MeasuerHeap(ContainsRecursively));
        _ExecuteMeasureHeap(numberOfMeasurements, structureSizeList.GetItem(i), "h_cl_100", MeasuerHeap(ContainsLinear)); 

        _ExecuteMeasureBSTree(numberOfMeasurements, structureSizeList.GetItem(i), "b_ar_100", MeasuerBSTree(AddRecursively));
        _ExecuteMeasureBSTree(numberOfMeasurements, structureSizeList.GetItem(i), "b_al_100", MeasuerBSTree(AddLoop)); 
        _ExecuteMeasureBSTree(numberOfMeasurements, structureSizeList.GetItem(i), "b_c_100", MeasuerBSTree(ContainsBSTree)); 
        _ExecuteMeasureBSTree(numberOfMeasurements, structureSizeList.GetItem(i), "b_r_100", MeasuerBSTree(Remove));
    }

    std::cout << "Zakonczony automatyczne pomiary." << std::endl;
}



void Measure::_ExecuteMeasureArrayList(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerIList action) {

    std::fstream file;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if(file.is_open()) {

            file << structureSize << ";;";
            while(numberOfMeasurements--) {

                file << _MeasureArrayList(structureSize, action) << ';';

                if(file.fail()) 
                { 
                    std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary." << std::endl; 
                    break; 
                } 
            }

            file << std::endl;
            file.close(); 

    } 
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 
    

}


long Measure::_MeasureArrayList(int structureSize, MeasuerIList action) {

    ArrayList list;
    struct timespec a, b;

    std::random_device realRandomNumber;
    std::default_random_engine randomEngine(realRandomNumber());
    std::uniform_int_distribution<int> forValues(1, INT_MAX);
    std::uniform_int_distribution<int> forPositions(1, structureSize - 1);

    //przygotowanie losowej struktury o zadanej wielkosci
    while(structureSize) {
        int randomValue = forValues(randomEngine);
        if(list.AddFront(randomValue) == 1)
            structureSize--;
    }
 
    int randomValue = forValues(randomEngine);
    int randomPosition = forPositions(randomEngine);

    if(action == MeasuerIList(AddBack)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.AddBack(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(AddFront)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.AddFront(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(AddOnPosition)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.AddOnPosition(randomValue, randomPosition);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(Contains)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.Contains(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveBack)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveBack();
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveFront)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveFront();
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveFromPosition)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveFromPosition(randomPosition);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveValue)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveValue(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }

    
    list.DeleteStructure();

    return (b.tv_nsec - a.tv_nsec);

}


void Measure::_ExecuteMeasureLinkedList(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerIList action) {

    std::fstream file;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if(file.is_open()) {

            file << structureSize << ";;";
            while(numberOfMeasurements--) {

                file << _MeasureLinkedList(structureSize, action) << ';';

                if(file.fail()) 
                { 
                    std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary." << std::endl; 
                    break; 
                } 
            }

            file << std::endl;
            file.close(); 

    } 
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 
    

}

long Measure::_MeasureLinkedList(int structureSize, MeasuerIList action) {

    LinkedList list;
    struct timespec a, b;

    std::random_device realRandomNumber;
    std::default_random_engine randomEngine(realRandomNumber());
    std::uniform_int_distribution<int> forValues(1, INT_MAX);
    std::uniform_int_distribution<int> forPositions(1, structureSize - 1);

    //przygotowanie losowej struktury o zadanej wielkosci
    while(structureSize) {
        int randomValue = forValues(randomEngine);
        if(list.AddFront(randomValue) == 1)
            structureSize--;
    }
 
    int randomValue = forValues(randomEngine);
    int randomPosition = forPositions(randomEngine);

    if(action == MeasuerIList(AddBack)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.AddBack(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(AddFront)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.AddFront(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(AddOnPosition)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.AddOnPosition(randomValue, randomPosition);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(Contains)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.Contains(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveBack)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveBack();
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveFront)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveFront();
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveFromPosition)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveFromPosition(randomPosition);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerIList(RemoveValue)) {

        clock_gettime(CLOCK_REALTIME, &a);
        list.RemoveValue(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }

    
    list.DeleteStructure();

    return (b.tv_nsec - a.tv_nsec);

}



void Measure::_ExecuteMeasureHeap(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerHeap action) {

    std::fstream file;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if(file.is_open()) {

            file << structureSize << ";;";
            while(numberOfMeasurements--) {
                file << _MeasureHeap(structureSize, action) << ';';

                if(file.fail()) 
                { 
                    std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary." << std::endl; 
                    break; 
                } 
            }

            file << std::endl;
            file.close(); 

    } 
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 
    

}

long Measure::_MeasureHeap(int structureSize, MeasuerHeap action) {

    HeapMax measureHeap;
    struct timespec a, b;

    std::random_device realRandomNumber;
    std::default_random_engine randomEngine(realRandomNumber());
    std::uniform_int_distribution<int> forValues(1, INT_MAX);

    //przygotowanie losowej struktury o zadanej wielkosci
    while(structureSize) {
        int randomValue = forValues(randomEngine);
        if(measureHeap.Push(randomValue) == 1)
            structureSize--;
    }
 
    int randomValue = forValues(randomEngine);

    if(action == MeasuerHeap(Push)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measureHeap.Push(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerHeap(Pop)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measureHeap.Pop();
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerHeap(ContainsRecursively)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measureHeap.ContainsRecursively(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerHeap(ContainsLinear)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measureHeap.ContainsLinear(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }

    
    measureHeap.DeleteStructure();

    return (b.tv_nsec - a.tv_nsec);

}


void Measure::_ExecuteMeasureBSTree(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerBSTree action) {

    std::fstream file;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if(file.is_open()) {

            file << structureSize << ";;";
            while(numberOfMeasurements--) {

                file << _MeasureBSTree(structureSize, action) << ';';

                if(file.fail()) 
                { 
                    std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary." << std::endl; 
                    break; 
                } 
            }

            file << std::endl;
            file.close(); 

    } 
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 
    

}

long Measure::_MeasureBSTree(int structureSize, MeasuerBSTree action) {

    BSTree measuerBSTree;
    struct timespec a, b;

    std::random_device realRandomNumber;
    std::default_random_engine randomEngine(realRandomNumber());
    std::uniform_int_distribution<int> forValues(1, INT_MAX);

    //przygotowanie losowej struktury o zadanej wielkosci
    while(structureSize) {
        int randomValue = forValues(randomEngine);
        if(measuerBSTree.AddLoop(randomValue) == 1)
            structureSize--;
    }
 
    int randomValue = forValues(randomEngine);

    if(action == MeasuerBSTree(AddRecursively)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measuerBSTree.AddRecursively(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerBSTree(AddLoop)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measuerBSTree.AddLoop(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerBSTree(ContainsBSTree)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measuerBSTree.Contains(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(action == MeasuerBSTree(Remove)) {

        clock_gettime(CLOCK_REALTIME, &a);
        measuerBSTree.Remove(randomValue);
        clock_gettime(CLOCK_REALTIME, &b);
    }

    
    measuerBSTree.DeleteStructure();

    return (b.tv_nsec - a.tv_nsec);

}
