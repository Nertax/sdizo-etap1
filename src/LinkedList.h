#include<iostream>
#include"IList.h"
#include"IStructure.h"

#ifndef LinkedList_h
#define LinkedList_h

struct LinkedListElement {

    public:
        int Value;
        LinkedListElement* Next;
        LinkedListElement* Prev;

};

class LinkedList : public IList, public IStructure {

    private:
        LinkedListElement* _head;
        LinkedListElement* _tail;
        int _size;

    public:
        LinkedList();
        ~LinkedList();

        int AddBack(int value);
        int AddFromFile(int value);
        int AddFront(int value);
        int AddOnPosition(int value, int position);
        bool Contains(int value);
        void DeleteStructure();
        int GetItemPosition(int value);
        int GetSize() { return _size; }
        void Print();
        int RemoveBack();
        int RemoveFront();
        int RemoveFromPosition(int position);
        int RemoveValue(int value);
};

#endif

/*
TODO:

mozna przyspieszyc dodawanie/usuwanie w linkedlist na zadana pozycje poprzez sprawdzenie
czy ta pozycja jest blizej poczatku czy konca wzgledem polowy dlugosci i iterowac sie
od head/tail w zaleznosci od tego gdzie blizej

mozna przyspieszyc usuwanie po wartosci w linkedlist piszac to w jednej funkcji
bo tak dwa razy przechodzimy po tych samych elementach

*/