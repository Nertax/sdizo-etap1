#include"Menu.h"


void Menu::Start() {

    _PrintStart();
    _MenuLoop();
}


//napis startowy w glownym menu
void Menu::_PrintStart() {

    std::cout << "Program zostal uruchomiony w trybie recznego korzystania ze struktur danych. " << std::endl
              << "Mozna w nim miedzy innymi testowac poprawnosc implementacji." << std::endl
              << "Aby wyswietlic pomoc w trakcie pracy wpisz h/help" << std::endl << std::endl;
    
}

//napis helpa z glownego menu
void Menu::_PrintHelp() {

    std::cout << "Nalezy wybrac strukture danych: " << std::endl
              << "ArrayList (tablica) - wpisz a/arraylist" << std::endl
              << "LinkedList (lista dwukierunkowa) - wpisz l/linkedlist" << std::endl
              << "BSTree (binarne drzewo poszukiwan) - wpisz b/bstree" << std::endl
              << "Heap (kopiec) - wpisz heap" << std::endl
              << "Aby wyjsc z programu wpisz q/quit" << std::endl << std::endl;
    
}

//napis helpa z menu array listy
void Menu::_PrintArrayListHelp() {


    std::cout << "Aktualnie pracujesz na ArrayList (tablica), mozliwe akcje to: " << std::endl
              << "AddFront - wpisz af/addfront i podaj liczbe" << std::endl
              << "AddBack - wpisz ab/addback i podaj liczbe" << std::endl
              << "AddOnPosition - wpisz ap/addonposition i podaj liczbe, a nastepnie pozycje dla niej" << std::endl
              << "MakeFromFile - wpisz mff/makefromfile i podaj nazwe pliku" << std::endl
              << "Print - wpisz p/print" << std::endl
              << "DeleteStructure - wpisz d/delete" << std::endl
              << "Contains - wpisz c/contains i podaj liczbe" << std::endl
              << "GetItemPosition - wpisz g/getitemposition i podaj liczbe" << std::endl
              << "RemoveFront - wpisz rf/removefront" << std::endl
              << "RemoveBack - wpisz rb/removeback" << std::endl
              << "RemoveFromPosition - wpisz rp/removefromposition i podaj pozycje" << std::endl
              << "RemoveValue - wpisz rv/removevalue i podaj liczbe" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;


}

//napis helpa z menu linked listy
void Menu::_PrintLinkedListHelp() {

    std::cout << "Aktualnie pracujesz na LinkedList (lista dwukierunkowa), mozliwe akcje to: " << std::endl
              << "AddFront - wpisz af/addfront i podaj liczbe" << std::endl
              << "AddBack - wpisz ab/addback i podaj liczbe" << std::endl
              << "AddOnPosition - wpisz ap/addonposition i podaj liczbe, a nastepnie pozycje dla niej" << std::endl
              << "MakeFromFile - wpisz mff/makefromfile i podaj nazwe pliku" << std::endl
              << "Print - wpisz p/print" << std::endl
              << "DeleteStructure - wpisz d/delete" << std::endl
              << "Contains - wpisz c/contains i podaj liczbe" << std::endl
              << "GetItemPosition - wpisz g/getitemposition i podaj liczbe" << std::endl
              << "RemoveFront - wpisz rf/removefront" << std::endl
              << "RemoveBack - wpisz rb/removeback" << std::endl
              << "RemoveFromPosition - wpisz rp/removefromposition i podaj pozycje" << std::endl
              << "RemoveValue - wpisz rv/removevalue i podaj liczbe" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;


}

//napis helpa z menu drzewa bst
void Menu::_PrintBSTreeHelp() {

    std::cout << "Aktualnie pracujesz na BSTree (binarne drzewo poszukiwan), mozliwe akcje to: " << std::endl
              << "AddRecursively - wpisz ar/addrecursively i podaj liczbe" << std::endl
              << "AddLoop - wpisz al/addloop i podaj liczbe" << std::endl
              << "MakeFromFile - wpisz mff/makefromfile i podaj nazwe pliku" << std::endl
              << "PrintPreOrder - wpisz ppr/printpreorder" << std::endl
              << "PrintInOrder - wpisz pin/printinorder" << std::endl
              << "PrintPostOrder - wpisz ppo/printpostorder" << std::endl
              << "Contains - wpisz c/contains i podaj liczbe" << std::endl
              << "DeleteTree - wpisz d/delete" << std::endl
              << "Remove - wpisz r/remove i podaj liczbe do usuniecia" << std::endl
              << "PrintGraph - wpisz pg/printgraph" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;

}

//komunikat helpa z menu kopca
void Menu::_PrintHeapHelp() {

    std::cout << "Aktualnie pracujesz na Heap (kopiec), mozliwe akcje to: " << std::endl
              << "Push - wpisz pu/push i podaj liczbe" << std::endl
              << "Pop - wpisz po/pop" << std::endl
              << "PrintArray - wpisz pa/printarray" << std::endl
              << "PrintGraph - wpisz pg/printgraph" << std::endl
              << "MakeFromFile - wpisz mff/makefromfile i podaj nazwe pliku" << std::endl
              << "ContainsRecursively - wpisz cr/containsrecursively i podaj liczbe" << std::endl
              << "ContainsLinear - wpisz cl/containslinear i podaj liczbe" << std::endl
              << "DeleteStructure - wpisz d/delete" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;

}

//napis ze nie ma takiej funkcji
void Menu::_PrintNoSuchAction() {

    std::cout << "Nie znaleziono takiej akcji. Prosze wybrac ponownie." << std::endl
              << "Polecenie h/help wyswietla pomoc." << std::endl;

}

//glowna petla menu
void Menu::_MenuLoop() {

    while(1) {

        std::string action;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);


        if(action == "h" || action == "help")
            _PrintHelp();

        else if(action == "a" || action == "arraylist")
            _SubMenuArrayList();

        else if(action == "l" || action == "linkedlist")
            _SubMenuLinkedList();

        else if(action == "b" || action == "bstree")
            _SubMenuBSTree();

        else if(action == "heap")
            _SubMenuHeap();

        else if(action == "q" || action == "quit")
            return;

        else
            _PrintNoSuchAction();

    }


}

//petla podmenu arraylisty
void Menu::_SubMenuArrayList() {

    while(1) {

        std::string action, fileName;
        int a, b, ret;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintArrayListHelp();

        else if(action == "ab" || action == "addback") {
            std::cin >> a;
            ret = MenuArrayList.AddBack(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
        }

        else if(action == "af" || action == "addfront") {
            std::cin >> a;
            ret = MenuArrayList.AddFront(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
        }

        else if(action == "ap" || action == "addonposition") {
            std::cin >> a >> b;
            ret = MenuArrayList.AddOnPosition(a, b);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Nieprawidlowa pozycja" << std::endl;
        }

        else if(action == "mff" || action == "makefromfile") {
            std::cin >> fileName;
            _MakeFromFile(fileName, &MenuArrayList);
        }

        else if(action == "p" || action == "print") {
            MenuArrayList.Print();
        }

        else if(action == "d" || action == "delete") {
            MenuArrayList.DeleteStructure();
        }

        else if(action == "c" || action == "contains") {
            std::cin >> a;
            if(MenuArrayList.Contains(a))
                std::cout << "W strukturze jest taki element" << std::endl;
            else
                std::cout << "W strukturze nie ma takiego elementu" << std::endl;
        }

        else if(action == "g" || action == "getitemposition") {
            std::cin >> a;
            ret = MenuArrayList.GetItemPosition(a);
            if(ret == -1)
                std::cout << "Nie ma takiego elementu" << std::endl;
            else
                std::cout << "Pozycja elementu " << a << " to: " << ret << std::endl;
        }

        else if(action == "rf" || action == "removefront") {
            ret = MenuArrayList.RemoveFront();
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Lista jest pusta" << std::endl;
        }

        else if(action == "rb" || action == "removeback") {
            ret = MenuArrayList.RemoveBack();
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Lista jest pusta" << std::endl;
        }

        else if(action == "rp" || action == "removefromposition") {
            std::cin >> a;
            ret = MenuArrayList.RemoveFromPosition(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Nie mozna usunac ze wskazanej pozycji" << std::endl;
        }

       else if(action == "rv" || action == "removevalue") {
            std::cin >> a;
            ret = MenuArrayList.RemoveValue(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Nie ma elementu o wskazanej wartosci" << std::endl;
        }        
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }


}

//petla podmenu linkedlisty
void Menu::_SubMenuLinkedList() {

    while(1) {

        std::string action, fileName;
        int a, b, ret;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintLinkedListHelp();

        else if(action == "ab" || action == "addback") {
            std::cin >> a;
            ret = MenuLinkedList.AddBack(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
        }

        else if(action == "af" || action == "addfront") {
            std::cin >> a;
            ret = MenuLinkedList.AddFront(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
        }

        else if(action == "ap" || action == "addonposition") {
            std::cin >> a >> b;
            ret = MenuLinkedList.AddOnPosition(a, b);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Nieprawidlowa pozycja" << std::endl;
        }

        else if(action == "mff" || action == "makefromfile") {
            std::cin >> fileName;
            _MakeFromFile(fileName, &MenuLinkedList);
        }

        else if(action == "p" || action == "print") {
            MenuLinkedList.Print();
        }

        else if(action == "d" || action == "delete") {
            MenuLinkedList.DeleteStructure();
        }

        else if(action == "c" || action == "contains") {
            std::cin >> a;
            if(MenuLinkedList.Contains(a))
                std::cout << "W strukturze jest taki element" << std::endl;
            else
                std::cout << "W strukturze nie ma takiego elementu" << std::endl;
        }

        else if(action == "g" || action == "getitemposition") {
            std::cin >> a;
            ret = MenuLinkedList.GetItemPosition(a);
            if(ret == -1)
                std::cout << "Nie ma takiego elementu" << std::endl;
            else
                std::cout << "Pozycja elementu " << a << " to: " << ret << std::endl;
        }

        else if(action == "rf" || action == "removefront") {
            ret = MenuLinkedList.RemoveFront();
            if(ret == -1)
                std::cout << "Lista jest pusta" << std::endl;
        }

        else if(action == "rb" || action == "removeback") {
            ret = MenuLinkedList.RemoveBack();
            if(ret == -1)
                std::cout << "Lista jest pusta" << std::endl;
        }

        else if(action == "rp" || action == "removefromposition") {
            std::cin >> a;
            ret = MenuLinkedList.RemoveFromPosition(a);
            if(ret == -1)
                std::cout << "Nie mozna usunac ze wskazanej pozycji" << std::endl;
        }

        else if(action == "rv" || action == "removevalue") {
            std::cin >> a;
            ret = MenuLinkedList.RemoveValue(a);
            if(ret == -1)
                std::cout << "Nie ma elementu o wskazanej wartosci" << std::endl;
        }  
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }

}

//petla podmenu drzewa bst
void Menu::_SubMenuBSTree() {

        while(1) {

        std::string action, fileName;
        int a, ret;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintBSTreeHelp();

        else if(action == "ar" || action == "addrecursively") {
            std::cin >> a;
            ret = MenuBSTree.AddRecursively(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Element o takiej wartosci jest juz w drzewie" << std::endl;
        }
        
        else if(action == "al" || action == "addloop") {
            std::cin >> a;
            ret = MenuBSTree.AddLoop(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Element o takiej wartosci jest juz w drzewie" << std::endl;
        }

        else if(action == "mff" || action == "makefromfile") {
            std::cin >> fileName;
            _MakeFromFile(fileName, &MenuBSTree);
        }

        else if(action == "ppr" || action == "printpreorder") {
            MenuBSTree.PrintPreOrder();
        }

        else if(action == "pin" || action == "printinorder") {
            MenuBSTree.PrintInOrder();
        }

        else if(action == "ppo" || action == "printpostorder") {
            MenuBSTree.PrintPostOrder();
        }

        else if(action == "c" || action == "contains") {
            std::cin >> a;
            if(MenuBSTree.Contains(a))
                std::cout << "W strukturze jest taki element" << std::endl;
            else
                std::cout << "W strukturze nie ma takiego elementu" << std::endl;
        }

        else if(action == "d" || action == "delete") {
            MenuBSTree.DeleteStructure();
        }

        else if(action == "r" || action == "remove") {
            std::cin >> a;
            ret = MenuBSTree.Remove(a);
            if(ret == -1)
                std::cout << "Nie ma elementu o wskazanej wartosci" << std::endl;
        }

        else if(action == "pg" || action == "printgraph") {
            MenuBSTree.PrintGraph();
        }
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }

}

//petla podmenu kopiec
void Menu::_SubMenuHeap() {

        while(1) {

        std::string action, fileName;
        int a, ret;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintHeapHelp();

        else if(action == "pu" || action == "push") {
            std::cin >> a;
            ret = MenuHeap.Push(a);
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
        }

        else if(action == "po" || action == "pop") {
            ret = MenuHeap.Pop();
            if(ret == 0)
                std::cout << "Blad alokacji pamieci" << std::endl;
            else if(ret == -1)
                std::cout << "Kopiec jest pusty" << std::endl;
        }

        else if(action == "pa" || action == "printarray") {
            MenuHeap.PrintArray();
        }

        else if(action == "pg" || action == "printgraph") {
            MenuHeap.PrintGraph();
        }

        else if(action == "mff" || action == "makefromfile") {
            std::cin >> fileName;
            _MakeFromFile(fileName, &MenuHeap);
        }

        else if(action == "cr" || action == "containsrecursively") {
            std::cin >> a;
            if(MenuHeap.ContainsRecursively(a))
                std::cout << "W strukturze jest taki element" << std::endl;
            else
                std::cout << "W strukturze nie ma takiego elementu" << std::endl;
        }

        else if(action == "cl" || action == "containslinear") {
            std::cin >> a;
            if(MenuHeap.ContainsLinear(a))
                std::cout << "W strukturze jest taki element" << std::endl;
            else
                std::cout << "W strukturze nie ma takiego elementu" << std::endl;
        }

        else if(action == "d" || action == "delete") {
            MenuHeap.DeleteStructure();
        }
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }

}


void Menu::_MakeFromFile(std::string fileName, IStructure* structure) {

    int size, value;
    std::ifstream file;
    file.open(fileName.c_str());

    structure->DeleteStructure();

    if(file.is_open()) {
    
        file >> size; 
        if(file.fail()) 
            std::cout << "Nie udalo sie wczytac ilosci danych z poczatku pliku" << std::endl; 
        else
            for(int i = 0; i < size; i++) 
            { 
                file >> value; 
                if(file.fail()) 
                { 
                    std::cout << "Blad odczytu danych, udalo sie wczytac: " << i << " danych poprawnie, przerywam dalsze wczytywanie" << std::endl; 
                    break; 
                } 
                else 
                    if(structure->AddFromFile(value) == 0) {
                        std::cout << "Blad zwiazany z alokowaniem pamieci podczas dodawania do struktury, przerywam dalsze wczytywanie" << std::endl;
                        break;
                    } 
            } 

            file.close(); 
    } 
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 

}