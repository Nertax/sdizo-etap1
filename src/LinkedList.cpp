#include"LinkedList.h"

LinkedList::LinkedList() {

    _head = NULL;
    _tail = NULL;
    _size = 0;

}

LinkedList::~LinkedList() {

    DeleteStructure();
}

/* Metoda dodajaca element na koniec listy laczonej

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowy element
*/
int LinkedList::AddBack(int value) {

    if(_head == NULL) {

        _head = new (std::nothrow) LinkedListElement;
        if(_head == NULL) return 0;

        _head->Value = value;
        _head->Prev = NULL;
        _head->Next = NULL;
        _tail = _head;
        _size = 1;

    }
    else {

        LinkedListElement* temp = _tail;

        _tail = new (std::nothrow) LinkedListElement;
        if(_tail == NULL) return 0;

        _tail->Value = value;
        _tail->Next = NULL;
        _tail->Prev = temp;
        temp->Next = _tail;
        _size++;        
    }

    return 1;

}


/* Metoda uzywana podczas tworzenia struktury z pliku
   implementuje metode z interfejsu IStructure

   zwraca to samo co metoda wywolywana przez nia - AddBack
*/
int LinkedList::AddFromFile(int value) {

    return AddBack(value);
}


/* Metoda dodajaca element na poczatek listy laczonej

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowy element
*/
int LinkedList::AddFront(int value) {

    if(_head == NULL) {

        _head = new (std::nothrow) LinkedListElement;
        if(_head == NULL) return 0;

        _head->Value = value;
        _head->Prev = NULL;
        _head->Next = NULL;
        _tail = _head;
        _size = 1;

    }
    else {

        LinkedListElement* temp = _head;

        _head = new (std::nothrow) LinkedListElement;
        if(_head == NULL) return 0;

        _head->Value = value;
        _head->Prev = NULL;
        _head->Next = temp;
        temp->Prev = _head;
        _size++;
    }

    return 1;
}


/* Metoda dodajaca element na wybrana pozycje w liscie

   Pozycje numerujemy od ZERA tak jak w tablicach

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowy element
   zwraca -1 jezelie probujesz dodac element na bledna pozycje - < 0 lub > _size
*/
int LinkedList::AddOnPosition(int value, int position) {

     if(position >= 0 && position <= _size) {

        if(position == 0) {
            if(AddFront(value) == 0) return 0;

        }
        else if(position == _size) {
            if(AddBack(value) == 0) return 0;

        }
        else {

            LinkedListElement* temp = _head;

            for(int i = 0; i < position; i++)
                temp = temp->Next;
            
            LinkedListElement* tempNew = new (std::nothrow) LinkedListElement;
            if(tempNew == NULL) return 0;

            tempNew->Value = value;
            tempNew->Next = temp;
            tempNew->Prev = temp->Prev;
            temp->Prev->Next = tempNew;
            temp->Prev = tempNew;
            _size++; 

        }
    }
    else {

        //chcemy dodac element na bledna pozycje
        return -1;

    } 

    return 1;
    
}

/* Metoda sprawdzajaca czy w lisicie znajduje sie element o wskazanej wartosci,
   jezeli tak zwraca true, w przeciwnym razie false
*/
bool LinkedList::Contains(int value) {

    if(GetItemPosition(value) != -1)
        return true;
    else
        return false;
 
}

/* Usuwa cala liste */
void LinkedList::DeleteStructure() {

    //przechodzimy po calej liscie i usuwamy po drodze wszystkie elementy z pamieci
    LinkedListElement* prev;

    while(_head != NULL) {

        prev = _head;
        _head = _head->Next;
        delete prev;
    }

    _tail = NULL;
    _size = 0;
}


/* Metoda znajdujaca pierwszy element listy o zadanej wartosci i zwracajaca jego numer
   liczac od zera, jesli nie ma takiego elementu zwraca -1
*/
int LinkedList::GetItemPosition(int value) {

    LinkedListElement* temp = _head;

    for(int i = 0; temp != NULL; i++) {
        if(temp->Value == value)
            return i;

        temp = temp->Next;
    }

    return -1;
}

/* Metoda wypisujaca zawartosc listy na stdout */
void LinkedList::Print() {

    LinkedListElement* temp = _head;

    	while(temp != NULL) {
            std::cout << temp->Value << " ";
            temp = temp->Next;
	    }

	std::cout << std::endl; 
}


/* Metoda usuwająca ostatni element z listy laczonej

   zwraca 1 jezeli operacja sie powiedzie
   zwraca -1 jezeli probujesz usunac ostatni element z pustej listy
*/
int LinkedList::RemoveBack() {

    if(_size == 1) {

        delete _head;
        _head = NULL;
        _tail = NULL;
        _size = 0;
    }
    else if(_size > 1) {

        LinkedListElement* temp = _tail->Prev;

        delete _tail;
        _tail = temp;
        _tail->Next = NULL;
        _size--;

    } 
    else {

        //usuwanie z pustej listy
        return -1;

    }

    return 1;
}


/* Metoda usuwająca pierwszy element z listy laczonej

   zwraca 1 jezeli operacja sie powiedzie
   zwraca -1 jezeli probujesz usunac ostatni element z pustej listy
*/
int LinkedList::RemoveFront() {

     if(_size == 1) {

        delete _head;
        _head = NULL;
        _tail = NULL;
        _size = 0;
    }
    else if(_size > 1) {

        LinkedListElement* temp = _head->Next;

        delete _head;
        _head = temp;
        _head->Prev = NULL;
        _size--;

    }
    else {

        //usuwanie z pustej listy
        return -1;

    }

    return 1;

}


/* Metoda usuwajaca element z wybranej pozycji listy laczonej

   Pozycje numerujemy od ZERA tak jak w tablicach

   zwraca 1 jezeli operacja sie powiedzie
   zwraca -1 jezelie probujesz usunac element z blednej pozycji - < 0 lub >= _size
*/
int LinkedList::RemoveFromPosition(int position) {

    if(position >= 0 && position < _size) {


        if(position == 0) {
            //sprawdzanie -1 dla RemoveFront nie jest konieczne 
            //bo z tego miejsca nie da sie doprowadzic do takiej sytuacji
            RemoveFront();
        }
        else if(position == _size - 1) {
            //sprawdzanie -1 dla RemoveFront nie jest konieczne 
            //bo z tego miejsca nie da sie doprowadzic do takiej sytuacji
            RemoveBack();
        }
        else {

            LinkedListElement* temp = _head;

            for(int i = 0; i < position; i++)
                temp = temp->Next;

            temp->Prev->Next = temp->Next;
            temp->Next->Prev = temp->Prev;
            delete temp;
            _size--;

        }
    }
    else {

        //usuwanie z niedozwolonej pozycji
        return -1;

    } 

    return 1;

}



/* Metoda usuwająca pierwszy napotkany element z listy laczonej o zadanej wartosci

   zwraca 1 jezeli operacja sie powiedzie
   zwraca -1 jezeli probujesz usunac element ktorego nie ma w liscie
*/
int LinkedList::RemoveValue(int value) {

    int position = GetItemPosition(value);
    if(position == -1) return -1;

    RemoveFromPosition(position);
    //sprawdzanie dla RemoveFromPosition dla -1 nie jest konieczne
    //bo taka sytuacja z tego miejsca nigdy nie nastapi

    return 1;
}