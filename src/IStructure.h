#ifndef IStructure_h
#define IStructure_h

class IStructure {

    public:
        virtual int AddFromFile(int value) = 0;
        virtual void DeleteStructure() = 0;
};

#endif