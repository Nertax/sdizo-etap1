#include<string>
#include<iostream>
#include"IStructure.h"

#ifndef BSTree_h
#define BSTree_h

class BSTNode {

    public:
        int Value;
        BSTNode* Left;
        BSTNode* Right;
        BSTNode* Prev;

        BSTNode();
        BSTNode(int value, BSTNode* left, BSTNode* right, BSTNode* prev);

};

class BSTree : public IStructure {

    private:
        BSTNode* _root;
        std::string _cr, _cp, _cl;

        int _AddInSubtreeRecursively(int value, BSTNode* root);
        void _DeleteSubtree(BSTNode* root);
        BSTNode* _FindNextSmallestNode(BSTNode* nextThanThisNode);
        BSTNode* _FindNode(int value);
        BSTNode* _FindNodeInSubtree(int value, BSTNode* subtreeNode);
        BSTNode* _FindSmallestNodeInSubtree(BSTNode* root);
        void _PrintGraph(std::string sp, std::string sn, BSTNode * v);
        void _PrintInOrder(BSTNode* root);
        void _PrintPostOrder(BSTNode* root);
        void _PrintPreOrder(BSTNode* root);
        void _RemoveNode(BSTNode* nodeToRemove);


    public:
        BSTree();
        ~BSTree();

        int AddLoop(int value);
        int AddFromFile(int value);
        int AddRecursively(int value);
        bool Contains(int value);
        void DeleteStructure();
        void PrintGraph();
        void PrintInOrder();
        void PrintPostOrder();
        void PrintPreOrder();
        int Remove(int value);
};

#endif

