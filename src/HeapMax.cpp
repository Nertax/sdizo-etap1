#include"HeapMax.h"

HeapMax::HeapMax() {
    
    _size = 0;
    _heapArray = NULL;
    _cr = "  ";
    _cl = "  ";
    _cp = "  ";
}

HeapMax::~HeapMax() {

    DeleteStructure();
}


/* Metoda uzywana podczas tworzenia struktury z pliku
   implementuje metode z interfejsu IStructure

   zwraca to samo co metoda wywolywana przez nia - Push
*/
int HeapMax::AddFromFile(int value) {

    return Push(value);   
}

/* Metoda wyszukiwania liniowego czy dany element znajduje sie w kopcu
   dziala jak wyszukiwanie w tablicy

   zwraca true jesli w kopcu jest dany element
   zwraca false w przeciwnym wypadku
*/
bool HeapMax::ContainsLinear(int value) {

    if(_size >= 1) {

        if(_heapArray[0] < value)
            return false;

        for(int i = 0; i < _size; i++)
            if(_heapArray[i] == value)
                return true;

        return false;
    }
    else
        return false;
}


/* Metoda wywolujaca rzeczywista rekurencyjna metode wyszukiwania - _ContainsRecursively 
   z odpowiednimi parametrami
*/
bool HeapMax::ContainsRecursively(int value) {

    return _ContainsRecursively(value, 0);
}

/* Usuwa caly kopiec*/
void HeapMax::DeleteStructure() {

    delete[] _heapArray;
    _heapArray = NULL;
    _size = 0;
}


/* Metoda usuwajaca element ze szczytu kopca, najpierw kopiuje ostatni element na poczatek tablicy
   po czym wywoluje rekurencyjna metode naprawy kopca w dol
   aby byl spelniony warunek kopca po usunieciu elementu

   W tle tablica kopca jest realokowana i przekopiowywana

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie usuwania, nie uda jej sie zaalokowac miejsca na nowa tablice
   zwraca -1 jezeli probujesz usunac element z pustego kopca
*/
int HeapMax::Pop() {

    if(_size == 1) {

        delete[] _heapArray;
        _heapArray = NULL;
        _size = 0;
    }
    else if(_size > 1) {

        int* newArray = new (std::nothrow) int[_size - 1];
        if(newArray == NULL) return 0;

        newArray[0] = _heapArray[_size - 1];
        
        for(int i = 1; i < _size - 1; i++)
            newArray[i] = _heapArray[i];

        delete[] _heapArray;
        _heapArray = newArray;
        _size--;

        //naprawa kopca
        _HeapMaxFixDown(0);

    }
    else {

        //usuwanie z pustego kopca
        return -1;

    }

    return 1;

}


/* Metoda wypisujaca zawartosc kopca w postaci listy na stdout */
void HeapMax::PrintArray() {

    for(int i = 0; i < _size; i++)
        std::cout << _heapArray[i] << " ";

    std::cout << std::endl;

}

/* Metoda wywolujaca rzeczywista rekurencyjna metode wypisywania - _PrintGraph 
   z odpowiednimi parametrami
*/
void HeapMax::PrintGraph() {

    _PrintGraph("","", 0);
}

/* Metoda dodajaca element do kopca, najpierw dodaje go na koniec tablicy
   po czym wywoluje rekurencyjna metode naprawy kopca w gore
   aby byl spelniony warunek kopca po dodaniu elementu

   W tle tablica kopca jest realokowana i przekopiowywana

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowa tablice
*/
int HeapMax::Push(int value) {

    if(_size == 0) {

        _heapArray = new (std::nothrow) int[1];
        if(_heapArray == NULL) return 0;

        _heapArray[0] = value;
        _size++;

    }
    else {

        int* newArray = new (std::nothrow) int[_size + 1];
        if(newArray == NULL) return 0;
        
        for(int i = 0; i < _size; i++)
            newArray[i] = _heapArray[i];

        newArray[_size] = value;
        _size++;

        delete[] _heapArray;
        _heapArray = newArray;

        //naprawa kopca
        _HeapMaxFixUp(_size - 1);
    }

    return 1;

}

/* Metoda rekurencyjna wyszukiwania czy dany element znajduje sie w kopcu
   wezel jest reprezentowany przez index

   zwraca true jesli w danym poddrzewie jest dany element
   zwraca false w przeciwnym wypadku
*/
bool HeapMax::_ContainsRecursively(int value, int index) {


   /* if(index < _size) {
        if(_heapArray[index] < value)
            return false;
        else if(_heapArray[index] == value)
            return true;
    
        int leftChild = 2 * index + 1;
        int rightChild = 2 * index + 2;

        if(leftChild >= _size)
            return false;
        else if(_ContainsRecursively(value, leftChild))
            return true;
        else if(rightChild >= _size)
            return false;
        else
            return _ContainsRecursively(value, rightChild);
    }
    else
        return false;*/


    if(index < _size) {

        if(_heapArray[index] < value)
            return false;
        else if(_heapArray[index] == value)
            return true;
        else return _ContainsRecursively(value, 2 * index + 1) || _ContainsRecursively(value, 2 * index + 2);
    }
    else
        return false;
    
}


/* Metoda rekurencyjna implementujaca algorytm naprawy kopca w dol,
   uzywana przy Pop 
*/
void HeapMax::_HeapMaxFixDown(int index) {

    int leftChild = 2 * index + 1;
    int rightChild = 2 * index + 2;

    if(leftChild < _size) {

        if(rightChild < _size) {

            if(_heapArray[index] < _heapArray[leftChild] || _heapArray[index] < _heapArray[rightChild]) {

                if(_heapArray[leftChild] > _heapArray[rightChild]) {

                    int temp = _heapArray[index];
                    _heapArray[index] = _heapArray[leftChild];
                    _heapArray[leftChild] = temp;
                    _HeapMaxFixDown(leftChild);

                }
                else {

                    int temp = _heapArray[index];
                    _heapArray[index] = _heapArray[rightChild];
                    _heapArray[rightChild] = temp;
                    _HeapMaxFixDown(rightChild);

                }
            }
        }
        else {

            if(_heapArray[index] < _heapArray[leftChild]) {

                int temp = _heapArray[index];
                _heapArray[index] = _heapArray[leftChild];
                _heapArray[leftChild] = temp;
            }
        }
    }
}

/* Metoda rekurencyjna implementujaca algorytm naprawy kopca w gore,
   uzywana przy Push 
*/
void HeapMax::_HeapMaxFixUp(int index) {

    if(index != 0) {

        int parentIndex = (index - 1)/2;

        if(_heapArray[parentIndex] < _heapArray[index]) {

            int temp = _heapArray[parentIndex];
            _heapArray[parentIndex] = _heapArray[index];
            _heapArray[index] = temp;

            _HeapMaxFixUp(parentIndex);

        }
    }
}

/* Metoda rekurencyjna wypisujaca zawartosc kopca w postaci grafu na stdout */
void HeapMax::_PrintGraph(std::string sp, std::string sn, int index) {
    //http://eduinf.waw.pl/inf/alg/001_search/0112.php

  std::string s;

  if(index < _size)
  {
    s = sp;
    if(sn == _cr) s[s.length() - 2] = ' ';
    _PrintGraph(s +_cp, _cr, 2*index+2);

    s = s.substr(0,sp.length()-2);
    std::cout << s << sn << _heapArray[index] << std::endl;

    s = sp;
    if(sn == _cl) s[s.length() - 2] = ' ';
    _PrintGraph(s +_cp,_cl, 2*index+1);
  }
}