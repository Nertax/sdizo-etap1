#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>
#include"ArrayList.h"
#include"LinkedList.h"
#include"BSTree.h"
#include"HeapMax.h"
#include"IStructure.h"


#ifndef Menu_h
#define Menu_h

class Menu {

    private:
        ArrayList MenuArrayList;
        LinkedList MenuLinkedList;
        BSTree MenuBSTree;
        HeapMax MenuHeap;

        void _MenuLoop();

        void _PrintStart();
        void _PrintHelp();
        void _PrintArrayListHelp();
        void _PrintLinkedListHelp();
        void _PrintBSTreeHelp();
        void _PrintHeapHelp();
        void _PrintNoSuchAction();
        
        void _SubMenuBSTree();
        void _SubMenuLinkedList();
        void _SubMenuArrayList();
        void _SubMenuHeap();

        void _MakeFromFile(std::string fileName, IStructure* structure);



    public:
        void Start();


};

#endif

