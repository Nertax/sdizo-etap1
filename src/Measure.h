#include<iostream>
#include<fstream>
#include<time.h>
#include<random>
#include<algorithm>
#include<climits>
#include"ArrayList.h"
#include"LinkedList.h"
#include"BSTree.h"
#include"HeapMax.h"

#ifndef Measure_h
#define Measure_h


enum MeasuerIList {

    AddBack,
    AddFront,
    AddOnPosition,
    Contains,
    RemoveBack,
    RemoveFront,
    RemoveFromPosition,
    RemoveValue

};

enum MeasuerHeap {

    Push,
    Pop,
    ContainsRecursively,
    ContainsLinear

};

enum MeasuerBSTree {

    AddRecursively,
    AddLoop,
    ContainsBSTree,
    Remove

};


class Measure {

    private:
        ArrayList MenuArrayList;
        LinkedList MenuLinkedList;
        BSTree MenuBSTree;
        HeapMax MenuHeap;

        void _AutoMeasurements();
        void _PrintHelp();
        void _PrintStart();
        void _MeasureMenuLoop();
        void _PrintNoSuchAction();
        void _PrintAutoMeasurements();
        void _PrintArrayListHelp();
        void _PrintLinkedListHelp();
        void _PrintHeapHelp();
        void _PrintBSTreeHelp();
        long _MeasureArrayList(int structureSize, MeasuerIList action);
        long _MeasureLinkedList(int structureSize, MeasuerIList action);
        long _MeasureHeap(int structureSize, MeasuerHeap action);
        long _MeasureBSTree(int structureSize, MeasuerBSTree action);
        void _MeasureArrayListMenu();
        void _MeasureLinkedListMenu();
        void _MeasureHeapMenu();
        void _MeasureBSTreeMenu();
        void _ExecuteMeasureLinkedList(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerIList action);
        void _ExecuteMeasureArrayList(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerIList action);
        void _ExecuteMeasureHeap(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerHeap action);
        void _ExecuteMeasureBSTree(int numberOfMeasurements, int structureSize, std::string fileName, MeasuerBSTree action);

    public:

        void Start();
};

#endif