#include"BSTree.h"

//public:

BSTNode::BSTNode() {

    Value = 0;
    Left = NULL;
    Right = NULL;
    Prev = NULL;


}

BSTNode::BSTNode(int value, BSTNode* left, BSTNode* right, BSTNode* prev) {

    Value = value;
    Left = left;
    Right = right;
    Prev = prev;


}


BSTree::BSTree() { 
    _root = NULL;
    _cr = "  ";
    _cl = "  ";
    _cp = "  ";
    

}

BSTree::~BSTree() {

    DeleteStructure();

}

/* Metoda dodajaca element do drzewa, za pomoca petlowego przechodzenia po drzewie
   w poszukiwaniu odpowiedniego miejsca dla elementu

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowy element
   zwraca -1 jezeli zadany element juz wystepuje w drzewie
*/
int BSTree::AddLoop(int value) {

    BSTNode* temp = _root;

    //jezeli drzewo puste
    if(_root == NULL) {
        _root = new (std::nothrow) BSTNode(value, NULL, NULL, NULL);
        if(_root == NULL) return 0;
        else return 1;
    }
    else {
        while(1) {
            if(value == temp->Value)
                return -1;

            else if(value < temp->Value) {
                if(temp->Left == NULL) {

                    temp->Left = new (std::nothrow) BSTNode(value, NULL, NULL, NULL);
                    if(temp->Left == NULL) return 0;

                    temp->Left->Prev = temp;
                    return 1;
                }
                else
                    temp = temp->Left;


            } 
            else {
                if(temp->Right == NULL) {

                    temp->Right = new (std::nothrow) BSTNode(value, NULL, NULL, NULL);
                    if(temp->Right == NULL) return 0;

                    temp->Right->Prev = temp;
                    return 1;
                }
                else
                    temp = temp->Right;
            } 
        }
    }

}

/* Metoda dodajaca element do drzewa, za pomoca rekurencyjnego dodawania do odpowiedniego
   poddrzewa, uruchamia metode  _AddInSubtreeRecursively z odpowiednimi parametrami

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowy element
   zwraca -1 jezeli zadany element juz wystepuje w drzewie
*/
int BSTree::AddRecursively(int value) {

    //jezeli drzewo puste
    if(_root == NULL) {
        _root = new (std::nothrow) BSTNode(value, NULL, NULL, NULL);
        if(_root == NULL) return 0;
        else return 1;
    }
    else
        return _AddInSubtreeRecursively(value, _root);

}

/* Metoda uzywana podczas tworzenia struktury z pliku
   implementuje metode z interfejsu IStructure

   jezeli dana wartosc juz jest w drzewie, to wartosc nie doda sie do drzewa
   jednak podczas wczytywania nie spowoduje to zadnego bledu/komunikatu

   zwraca to samo co metoda wywolywana przez nia - AddLoop
*/
int BSTree::AddFromFile(int value) {

    return AddLoop(value);

}

/* Metoda sprawdzajaca czy w drzewie znajduje sie element o wskazanej wartosci,
   jezeli tak zwraca true, w przeciwnym razie false
*/
bool BSTree::Contains(int value) {

    if(_FindNode(value) == NULL)
        return false;
    else   
        return true;


}

/* Usuwa cale drzewo, wywoluje metode _DeleteSubtree */
void BSTree::DeleteStructure() {

    _DeleteSubtree(_root);
    _root = NULL;

}

/* Metoda wypisujaca zawartosc drzewa w postaci grafu na stdout,
   uruchamia metode _PrintGraph z odpowiednimi parametrami
*/
void BSTree::PrintGraph() {
    _PrintGraph("","", _root);
}

/* Metoda wypisujaca zawartosc drzewa w postaci listy na stdout
   - wersja in order, uruchamia metode _PrintInOrder z odpowiednimi parametrami
*/
void BSTree::PrintInOrder() {

    _PrintInOrder(_root);
    std::cout << std::endl;

}

/* Metoda wypisujaca zawartosc drzewa w postaci listy na stdout
   - wersja post order, uruchamia metode _PrintPostOrder z odpowiednimi parametrami
*/
void BSTree::PrintPostOrder() {

    _PrintPostOrder(_root);
    std::cout << std::endl;

}

/* Metoda wypisujaca zawartosc drzewa w postaci listy na stdout
   - wersja pre order, uruchamia metode _PrintPreOrder z odpowiednimi parametrami
*/
void BSTree::PrintPreOrder() {

    _PrintPreOrder(_root);
    std::cout << std::endl;

}


/* Metoda usuwająca element z drzewa o zadanej wartosci
   uruchamia metode _RemoveNode po wczesniejszym znalezieniu
   odpowiedniego wezla za pomoca metody _FindNode

   zwraca 1 jezeli operacja sie powiedzie
   zwraca -1 jezeli probujesz usunac wartosc ktorej nie ma w drzewie
*/
int BSTree::Remove(int value) {

    BSTNode* searchingResult = _FindNode(value);

    if(searchingResult != NULL) {
        _RemoveNode(searchingResult);
        return 1;
    }
    else {

        //nie ma takiej wartosci w drzewie
        return -1;
    
    }
}


//private:


/* Metoda dodajaca element do drzewa, za pomoca rekurencyjnego dodawania do odpowiedniego
   poddrzewa

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie dodawania, nie uda jej sie zaalokowac miejsca na nowy element
   zwraca -1 jezeli zadany element juz wystepuje w drzewie
*/
int BSTree::_AddInSubtreeRecursively(int value, BSTNode* root) {

     if(value == root->Value)
        return -1;
     else if(value < root->Value) {
        if(root->Left == NULL) {
            root->Left = new (std::nothrow) BSTNode(value, NULL, NULL, NULL);
            if(root->Left == NULL) return 0;
            root->Left->Prev = root;
            return 1;
        }
        else
            return _AddInSubtreeRecursively(value, root->Left);
     }
    else {
        if(root->Right == NULL) {
            root->Right = new (std::nothrow) BSTNode(value, NULL, NULL, NULL);
            if(root->Right == NULL) return 0;
            root->Right->Prev = root;
            return 1;
        }
        else
            return _AddInSubtreeRecursively(value, root->Right);
    }
}

/* Usuwa rekurencyjnie cale drzewo */
void BSTree::_DeleteSubtree(BSTNode* root) {

    if(root != NULL) {

        _DeleteSubtree(root->Left);
        _DeleteSubtree(root->Right);

        delete root;
    }
}


/* Metoda znajdowania nastepnika w drzewie, uzywa metody 
   _FindSmallestNodeInSubtree

   zwraca wskaznik na wezel bedacy nastepnikiem zadanego wezla, w przypadku jego braku zwraca NULL
*/
BSTNode* BSTree::_FindNextSmallestNode(BSTNode* nextThanThisNode) {

    if(nextThanThisNode->Right != NULL) {
        return _FindSmallestNodeInSubtree(nextThanThisNode->Right);

    }
    else {

        while(nextThanThisNode->Prev != NULL) {

            if(nextThanThisNode->Prev->Left == nextThanThisNode)
                return nextThanThisNode->Prev;
            else
                nextThanThisNode = nextThanThisNode->Prev;

        }

        return NULL;

    }
}


/* Metoda znajdujaca element o zadanej wartosci, wywoluje
   metode _FindNodeInSubtree z odpowiednimi parametrami 

   zwraca wskaznik na wezel o zadanej wartosci, w przypadku jego braku zwraca NULL
*/
BSTNode* BSTree::_FindNode(int value) {

    if(_root == NULL)
        return NULL;
    else {

        BSTNode* temp = _root;

        while(temp != NULL) {

            if(value == temp->Value)
                return temp;
            else if(value < temp->Value)
                temp = temp->Left;
            else
                temp = temp->Right;
        }

        return NULL;

    }



    //return _FindNodeInSubtree(value, _root);

}

/* Metoda rekurencyjna znajdowania element we wskazanym poddrzewie
   o zadanej wartosci

   zwraca wskaznik na wezel o zadanej wartosci, w przypadku jego braku zwraca NULL
*/
BSTNode* BSTree::_FindNodeInSubtree(int value, BSTNode* subTreeNode) {

    if(subTreeNode == NULL)
        return NULL;
    else if(subTreeNode->Value == value)
        return subTreeNode;
    else {
        BSTNode* searchingInLeftSubTreeResult = _FindNodeInSubtree(value, subTreeNode->Left);

        if(searchingInLeftSubTreeResult != NULL)
            return searchingInLeftSubTreeResult;
        else
            return _FindNodeInSubtree(value, subTreeNode->Right);
    }


}

/* Metoda rekurencyjna znajdowania najmniejszego element we wskazanym poddrzewie

   zwraca wskaznik na wezel z najmniejszym elementem w poddrzewie
*/
BSTNode* BSTree::_FindSmallestNodeInSubtree(BSTNode* root) {

    if(root->Left == NULL) 
        return root;
    else
        return _FindSmallestNodeInSubtree(root->Left);

}

/* Metoda rekurencyjna wypisujaca zawartosc drzewa w postaci grafu na stdout */
void BSTree::_PrintGraph(std::string sp, std::string sn, BSTNode * v) {
    //http://eduinf.waw.pl/inf/alg/001_search/0112.php

  std::string s;

  if(v)
  {
    s = sp;
    if(sn == _cr) s[s.length() - 2] = ' ';
    _PrintGraph(s + _cp, _cr, v->Right);

    s = s.substr(0,sp.length()-2);
    std::cout << s << sn << v->Value << std::endl;

    s = sp;
    if(sn == _cl) s[s.length() - 2] = ' ';
    _PrintGraph(s + _cp, _cl, v->Left);
  }
}

/* Metoda rekurencyjna wypisujaca zawartosc drzewa w postaci listy na stdout - wersja in order */
void BSTree::_PrintInOrder(BSTNode* root) {

    if(root != NULL) {
        _PrintInOrder(root->Left);
        std::cout << root->Value << " ";
        _PrintInOrder(root->Right);

    }
}

/* Metoda rekurencyjna wypisujaca zawartosc drzewa w postaci listy na stdout - wersja post order */
void BSTree::_PrintPostOrder(BSTNode* root) {

    if(root != NULL) {
        _PrintPostOrder(root->Left);
        _PrintPostOrder(root->Right);
        std::cout << root->Value << " ";

    }
}

/* Metoda rekurencyjna wypisujaca zawartosc drzewa w postaci listy na stdout - wersja pre order */
void BSTree::_PrintPreOrder(BSTNode* root) {

    if(root != NULL) {
        std::cout << root->Value << " ";
        _PrintPreOrder(root->Left);
        _PrintPreOrder(root->Right);

    }
}


/* Metoda rekurencyjnie usuwajaca z drzewa wskazany wezel */
void BSTree::_RemoveNode(BSTNode* nodeToRemove) {

    if(nodeToRemove->Left == NULL && nodeToRemove->Right == NULL) {

        if(nodeToRemove->Prev == NULL) {
            delete nodeToRemove;
             _root = NULL;
        }
        else {

            if(nodeToRemove->Prev->Right == nodeToRemove)
                nodeToRemove->Prev->Right = NULL;
            else
                nodeToRemove->Prev->Left = NULL;

            delete nodeToRemove;
        }

    }
    else if(nodeToRemove->Left == NULL || nodeToRemove->Right == NULL) {

         if(nodeToRemove->Prev == NULL) {

            if(nodeToRemove->Right == NULL)
                _root = _root->Left;
            else
                _root = _root->Right;

            _root->Prev = NULL;
        }
        else if(nodeToRemove->Prev->Right == nodeToRemove) {

            if(nodeToRemove->Right == NULL) {
                nodeToRemove->Prev->Right = nodeToRemove->Left;
                nodeToRemove->Left->Prev = nodeToRemove->Prev;
            }
            else {
                nodeToRemove->Prev->Right = nodeToRemove->Right;
                nodeToRemove->Right->Prev = nodeToRemove->Prev;
            }
        }
        else {

            if(nodeToRemove->Right == NULL) {
                nodeToRemove->Prev->Left = nodeToRemove->Left;
                nodeToRemove->Left->Prev = nodeToRemove->Prev;
            }
            else {
                nodeToRemove->Prev->Left = nodeToRemove->Right;
                nodeToRemove->Right->Prev = nodeToRemove->Prev;
            }
        }

        delete nodeToRemove;
    }
    else {

        BSTNode* nextSmallest = _FindNextSmallestNode(nodeToRemove);
        //jezeli wejdzie do tego elsa to to na pewno nie bedzie null

        nodeToRemove->Value = nextSmallest->Value;
        _RemoveNode(nextSmallest);

    }
}