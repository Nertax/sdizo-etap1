#ifndef IList_h
#define IList_h

class IList {

    public:
        virtual int AddBack(int value) = 0;
        virtual int AddFront(int value) = 0;
        virtual int AddOnPosition(int value, int position) = 0;
        virtual bool Contains(int value) = 0;
        virtual int GetItemPosition(int value) = 0;
        virtual int GetSize() = 0;
        virtual void Print() = 0;
        virtual int RemoveBack() = 0;
        virtual int RemoveFront() = 0;      
        virtual int RemoveFromPosition(int position) = 0;
        virtual int RemoveValue(int value) = 0;
};

#endif
